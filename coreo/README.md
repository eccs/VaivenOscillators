La coreografía en el círculo consiste en dos grandes etapas:
* Planteamiento de movimientos y exploración de combinaciones de desplazamientos
* Acumulación de movimientos con todos en desplazamiento

# Reglas generales
1. Solo pueden haber 8 personas. 
2. A cada una de ellas le será asignado un número del 0 al 7. 
3. Ese número corresponde al número de su trayectoria y a su desfase de la cuenta de inicio. El 0 siempre empieza en el tiempo 1, el 1 siempre empieza en el tiempo 2, el 2 siempre empieza en el tiempo 3, etc.
4. No puede empezar a moverse antes de ese número.
5. Los bailarines siempre tienen que ver hacia el centro del círculo

# Secuencia de movimiento individual

_work in progres..._

# Estructura grupal

[Aquí está el archivo con la secuencia planteada](https://github.com/Escenaconsejo/VaivenOscillators/blob/master/circle/maqueta.txt)

## Generalidades
1. Hay 3 etapas
2. Cada etapa está formada de semi-etapas
3. Cada semi-etapa está notada como 1.0,1.1,1.2,2.1,2.3,3.5, etc. 
4. Para que cada semi-etapa termine será necesario que cada persona (0-7) complete un ciclo
5. Un ciclo es hacer su trayectoria de ida y vuelta; es decir, ir del punto de inicio al otro extremo y de regreso al punto inicial; ó dos veces su frase de movimiento. 

## Las etapas
* Etapa I: cada persona muestra su trayectoria. Caminando solamente. "1.0" columnas 1 a 16. 
* Etapa II: se empiezan a mostrar las posibilidades discretamente y poco a poco. Columna 19 a 92. 
* Etapa III: se forman la ilusión del círculo rodando por la sucesión desfasada de cada persona en el tiempo específico de su trayectoria (0-7). Explota junto con la acumulación progresiva de todos los movimientos de la frase hasta llegar a completarla y repetirla completa al menos dos veces.

### Completa
```
0: xx----------------||x------------x|------x------x|--x--x----|x--x------|x--------x|x--x--|x--x--|x--x||xx|xx|xx|xx|xx|xx|xx|xx|x----
1: --xx--------------||--x--------x--|x------x------|------x--x|--x--x----|------xx--|--x--x|x--x--|x--x||xx|xx|xx|xx|xx|xx|xx|xx|x----
2: ----xx------------||----x----x----|----x------x--|------x--x|----x--x--|----xx----|x--x--|--x--x|x--x||xx|xx|xx|xx|xx|xx|xx|xx|x----
3: ------xx----------||------xx------|--x------x----|x--x------|------x--x|--xx------|--x--x|--x--x|x--x||xx|xx|xx|xx|xx|xx|xx|xx|x----
4: --------xx--------||------xx------|x------x------|x--x------|x--x------|------xx--|x--x--|x--x--|x--x||xx|xx|xx|xx|xx|xx|xx|xx|x----
5: ----------xx------||----x----x----|----x------x--|----x--x--|--x--x----|----xx----|--x--x|x--x--|x--x||xx|xx|xx|xx|xx|xx|xx|xx|x----
6: ------------xx----||--x--------x--|------x------x|----x--x--|----x--x--|--xx------|x--x--|--x--x|x--x||xx|xx|xx|xx|xx|xx|xx|xx|x----
7: --------------xx--||x------------x|--x------x----|--x--x----|------x--x|--------xx|--x--x|--x--x|--xx||xx|xx|xx|xx|xx|xx|xx|xx|x----
```

### Etapa I
#### 1.1 Columna 1 a 17
Cada persona camina sola en su trayectoria

```
1	2	3	4	5	6	7	8	9	10	11	12	13	14	15	16	17	18
x	x	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-
-	-	x	x	-	-	-	-	-	-	-	-	-	-	-	-	-	-
-	-	-	-	x	x	-	-	-	-	-	-	-	-	-	-	-	-
-	-	-	-	-	-	x	x	-	-	-	-	-	-	-	-	-	-
-	-	-	-	-	-	-	-	x	x	-	-	-	-	-	-	-	-
-	-	-	-	-	-	-	-	-	-	x	x	-	-	-	-	-	-
-	-	-	-	-	-	-	-	-	-	-	-	x	x	-	-	-	-
-	-	-	-	-	-	-	-	-	-	-	-	-	-	x	x	-	-
```

### Etapa II
#### 2.1 
Columna 19 a 32
De afuera hacia adentro y de adentro y hacia afuera
(1 y 8, 2 y 7, 3 y 6, 4 y 5, y viceversa)

* Pares: Movimiento 1
* Nones: Movimiento 8
```
19	20	21	22	23	24	25	26	27	28	29	30	31	32
x	-	-	-	-	-	-	-	-	-	-	-	-	x
-	-	x	-	-	-	-	-	-	-	-	x	-	-
-	-	-	-	x	-	-	-	-	x	-	-	-	-
-	-	-	-	-	-	x	x	-	-	-	-	-	-
-	-	-	-	-	-	x	x	-	-	-	-	-	-
-	-	-	-	x	-	-	-	-	x	-	-	-	-
-	-	x	-	-	-	-	-	-	-	-	x	-	-
x	-	-	-	-	-	-	-	-	-	-	-	-	x
```

#### 2.2
Columna 33 a 46

* Pares: Movimiento 2
* Nones: Movimiento 7
```
33	34	35	36	37	38	39	40	41	42	43	44	45	46
-	-	-	-	-	-	x	-	-	-	-	-	-	x
x	-	-	-	-	-	-	x	-	-	-	-	-	-
-	-	-	-	x	-	-	-	-	-	-	x	-	-
-	-	x	-	-	-	-	-	-	x	-	-	-	-
x	-	-	-	-	-	-	x	-	-	-	-	-	-
-	-	-	-	x	-	-	-	-	-	-	x	-	-
-	-	-	-	-	-	x	-	-	-	-	-	-	x
-	-	x	-	-	-	-	-	-	x	-	-	-	-
```

#### 2.3
Columna 47 a 56
Parejas

* Pares: Movimiento 3
* Nones: Movimiento 6
```
47	48	49	50	51	52	53	54	55	56
-	-	x	-	-	x	-	-	-	-
-	-	-	-	-	-	x	-	-	x
-	-	-	-	-	-	x	-	-	x
x	-	-	x	-	-	-	-	-	-
x	-	-	x	-	-	-	-	-	-
-	-	-	-	x	-	-	x	-	-
-	-	-	-	x	-	-	x	-	-
-	-	x	-	-	x	-	-	-	-
```

##### 2.4
Columna 57 a 66

* Pares: Movimiento 4
* Nones: Movimiento 5
```
57	58	59	60	61	62	63	64	65	66
x	-	-	x	-	-	-	-	-	-
-	-	x	-	-	x	-	-	-	-
-	-	-	-	x	-	-	x	-	-
-	-	-	-	-	-	x	-	-	x
x	-	-	x	-	-	-	-	-	-
-	-	x	-	-	x	-	-	-	-
-	-	-	-	x	-	-	x	-	-
-	-	-	-	-	-	x	-	-	x
```

#### 2.5
Columna 67 a 76

* Pares: Movimiento 5
* Nones: Movimiento 4
```
67	68	69	70	71	72	73	74	75	76
x	-	-	-	-	-	-	-	-	x
-	-	-	-	-	-	x	x	-	-
-	-	-	-	x	x	-	-	-	-
-	-	x	x	-	-	-	-	-	-
-	-	-	-	-	-	x	x	-	-
-	-	-	-	x	x	-	-	-	-
-	-	x	x	-	-	-	-	-	-
-	-	-	-	-	-	-	-	x	x
```

#### 2.6
Columna 77 a 82

* Pares: Movimiento 6
* Nones: Movimiento 3
```
77	78	79	80	81	82
x	-	-	x	-	-
-	-	x	-	-	x
x	-	-	x	-	-
-	-	x	-	-	x
x	-	-	x	-	-
-	-	x	-	-	x
x	-	-	x	-	-
-	-	x	-	-	x
```

#### 2.7
Columna 83 a 88

* Pares: Movimiento 7
* Nones: Movimiento 2
```
83	84	85	86	87	88
x	-	-	x	-	-
x	-	-	x	-	-
-	-	x	-	-	x
-	-	x	-	-	x
x	-	-	x	-	-
x	-	-	x	-	-
-	-	x	-	-	x
-	-	x	-	-	x
```

#### 2.8
Columna 89 a 92
* Pares: Movimiento 8
* Nones: Movimiento 1
```
89	90	91	92
x	-	-	x
x	-	-	x
x	-	-	x
x	-	-	x
x	-	-	x
x	-	-	x
x	-	-	x
-	-	x	x
```

### Etapa III
#### 3.1
Columna 93 a 94

* Pares: Movimiento 7 y 8
* Impares: Movimiento 1 y 2

```
x	x
x	x
x	x
x	x
x	x
x	x
x	x
x	x
```

#### 3.2
Columna 95 a 96

* Pares: Movimiento 6, 7 y 8
* Impares: Movimiento 1, 2 y 3

```
x	x
x	x
x	x
x	x
x	x
x	x
x	x
x	x
```

#### 3.3
Columna 97 a 98

* Pares: Movimiento 5, 6, 7 y 8
* Impares: Movimiento 1, 2, 3 y 4

```
x	x
x	x
x	x
x	x
x	x
x	x
x	x
x	x
```

#### 3.4
Columna 99 a 100

* Pares: Movimiento 4, 5, 6, 7 y 8
* Impares: Movimiento 1, 2, 3, 4, y 5

```
x	x
x	x
x	x
x	x
x	x
x	x
x	x
x	x
```

#### 3.5
Columna 101 a 102

* Pares: Movimiento 3, 4, 5, 6, 7 y 8
* Impares: Movimiento 1, 2, 3, 4, 5 y 6

```
x	x
x	x
x	x
x	x
x	x
x	x
x	x
x	x
```

#### 3.6
Columna 103 a 104

* Pares: Movimiento 2, 3, 4, 5, 6, 7 y 8
* Impares: Movimiento 1, 2, 3, 4, 5, 6, 7

```
x	x
x	x
x	x
x	x
x	x
x	x
x	x
x	x
```

#### 3.7
Columna 105 a 106

* Pares: Movimiento 1, 2, 3, 4, 5, 6, 7 y 8
* Impares: Movimiento 1, 2, 3, 4, 5, 6, 7, y 8

```
x	x
x	x
x	x
x	x
x	x
x	x
x	x
x	x
```

#### 3.8
Columna 107 a 108

* Pares: Movimiento 1, 2, 3, 4, 5, 6, 7 y 8
* Impares: Movimiento 1, 2, 3, 4, 5, 6, 7, y 8

```
x	x
x	x
x	x
x	x
x	x
x	x
x	x
x	x
```
