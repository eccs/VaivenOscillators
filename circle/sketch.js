
var nt;

var N_T = 256; // Number of "ticks" per PI

var oscs = [];

var playing;

var textarea;

var buttonPlay;
var buttonClear;
var buttonExample;

var sliderSpeed;
var textSpeed;

var filenames = ["plain.txt", "default.txt"];
var lines = [];

function preload(){
	for(var i=0; i<filenames.length; i++){
		lines[filenames[i]] = loadStrings(filenames[i]);
	}

}

function setup() {
	var canvas = createCanvas(500,500);
	canvas.parent('sketch');

	playing = false;

	buttonExample = createButton("Load Example");
	buttonExample.parent('buttons');
	buttonExample.mousePressed(doLoadExample);

	buttonClear = createButton("Clear");
	buttonClear.parent('buttons');
	buttonClear.mousePressed(doClear);

	buttonPlay = createButton("Start!");
	buttonPlay.parent('buttons');
	buttonPlay.mousePressed(doPlay);

	sliderSpeed = createSlider(256,512,256,8);
	sliderSpeed.size(300,AUTO);
	sliderSpeed.parent('slider');
	sliderSpeed.changed(doUpdateSpeed);
//	sliderSpeed.mousePressed(doUpdateText);

	textSpeed = createSpan('');
	textSpeed.parent('slider');






	textarea = select('#inputtext');



	var N = 8;
	var osc;
	for(var i=0; i<N; i++){
		osc = new Oscillator(-i/N,-i*PI/N);
		oscs.push(osc);
	}

	doClear();


	/*
	var defaultProgram = "";
	for(var i=0; i<N; i++){
		for(var j=0; j<100; j++){
			defaultProgram += "-";		
		}
		defaultProgram += "\n";
	}
	textarea.html(defaultProgram);
	*/

	nt = 0;

}


function draw() {
	background(240);

	textSize(30);
	fill(80);
	text("Column: "+int(1+nt/N_T),20,50);
	text("Beat: "+int((nt%(N_T))/(N_T/8)+1),20,80);

	translate(width/2,height/2);
	for(var i=0; i<oscs.length;i++){

		oscs[i].update(nt);
		oscs[i].draw();
	}


	if(playing){
		nt++;
	}

	doUpdateText();
  
}


function loadFileIntoSequence(filename){
	parseLinesIntoSequence(lines[filename]);


}

function doUpdateText(){
	textSpeed.html("1/"+N_T);
}

function doUpdateSpeed(){
	N_T = int(sliderSpeed.value());
	doUpdateText();
	doPlay();

}

function doLoadExample(){
	loadFileIntoSequence("default.txt");
}
function doClear(){
	loadFileIntoSequence("plain.txt");
}

function doPlay(){
	nt = 0;
	playing = true;

	buttonPlay.html("Restart!");

	parseTextIntoSequence(textarea.value());
}

function parseTextIntoSequence(txt){
	var lines = split(txt,'\n');
	
	parseLinesIntoSequence(lines);


}

function parseLinesIntoSequence(lines){
	var character;
	var result;
	for(var i=0; i<oscs.length; i++){
		oscs[i].sequence = [];
		for(var j=0; j<lines[i].length; j++){
			character = lines[i].charAt(j);
			if(character=='x'){
				oscs[i].sequence.push(1);
			}
			else if(character=='-'){
				oscs[i].sequence.push(0);
			}
		}
		oscs[i].pointer = 0;
		oscs[i].resetPosition();
	}

	var newhtml = join(lines,'\n');
	textarea.html(newhtml);

}


function mousePressed(){
//	osc.enable(true);
}



var STATE_MOVING = 1;
var STATE_PAUSE = 0;
var STATE_MOVING_A = 1;
var STATE_PAUSE_B = 2;
var STATE_MOVING_B = 3;
var STATE_PAUSE_A = 4;

var STATE_WAITING_A = 5;



function Oscillator(phase,rotation){
	this.state = STATE_PAUSE;
	this.enabled = false;

	this.rotation = rotation;
	this.phase = phase;
	this.intphase = int(phase*N_T);

	this.amplitude = 200;

	this.position = -this.amplitude;

	this.sequence = [0,0,0,0,0,0,0,0,0,0];
//	this.sequence = [1,0,0,1,0,0,1,1,0,0];
	this.pointer = 0;


	this.setup = function(){
		var p = createP("");
		p.html("hola");

		var c = createCheckbox('');
		c.parent(p);

	}

	this.resetPosition = function(){
		this.position = -this.amplitude;
	}



	this.incPointer = function(){
		this.pointer = (this.pointer+1)%this.sequence.length;
	}

	this.nextEnabled = function(){
		this.enabled = (this.sequence[this.pointer]==1);
		this.incPointer();

	//	println(this.pointer+" " +this.enabled);
	}

	this.isIntTime = function(nt){
		return (N_T+nt+this.intphase)%N_T == 0;
	}


	this.update = function(nt){
		var t = nt/N_T;


		if(this.isIntTime(nt)){
			this.nextEnabled(); // Get next from sequence
			if(this.enabled){
				this.state = STATE_MOVING;
			}
			else{
				this.state = STATE_PAUSE;
			}
		}
		else{
		}


		if(this.state == STATE_MOVING){
			this.position = this.d(t);
		}

		/*

		if(this.dd(t)==0 || int(degrees(t))==0 || int(degrees(t))==180 || abs(this.position)==this.amplitude){
			fill(255,0,0);
		}
		else{
			fill(0);
		}
		*/


		/*

		switch(this.state){
			case STATE_MOVING:
				if(this.enabled){
					this.position = this.d(t);
				}

			//	if(int(degrees(t))==0 || int(degrees(t))==180){

//				if(this.aequal(this.position,this.amplitude,this.amplitude)){
				if(int(abs(this.position)) == this.amplitude){
					this.state = STATE_PAUSE;
//					println("Changed to pause");


				}
			break;
			case STATE_PAUSE:
//				if(this.enabled && this.aequal(this.position,this.d(t),this.amplitude)){
				if(this.enabled && (this.position > this.d(t)-1 && this.position<this.d(t)+1)){
					this.state = STATE_MOVING;
//					println("Start moving: "+this.position+" "+this.d(t));
				}
				break;
			break;

		}
		*/



	}

	this.d = function(t){
		return	int(-this.amplitude*cos((t + this.phase)*PI));
	}

	this.dd = function(t){
		return	int(this.amplitude*sin((t + this.phase)*PI));	
	}


	this.aequal = function(x,y,z){
	//	println(x+" "+y+" "+z);
		return (abs(x-y)/z)<0.001;
	}


	this.draw = function(){
		push();

		rotate(this.rotation);

		var a = this.amplitude;
		stroke(80);
		line(-a,0,a,0);

		var x;
		var div = 8;
		for(var i=0; i<div+1; i++){
			x = a*cos(i*PI/div);
			line(x,-a*0.05,x,a*0.05);

		}
		
	//	fill(80);
		var diameter = 22;
		ellipse(this.position,0,diameter,diameter);

		pop();

	}

	this.enable = function(b){
		this.enabled = b;
	}
		


}
